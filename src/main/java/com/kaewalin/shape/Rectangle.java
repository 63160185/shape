/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.shape;

/**
 *
 * @author ACER
 */
public class Rectangle extends Shape {

    private double w;
    private double h;

    public Rectangle(double w, double h) {
        super("Rectangle");
        this.w = w;
        this.h = h;

    }

    @Override
    public double calArea() {
        if (w == 0 || h == 0) {
            System.out.println("Width or height must more than 0 !!!");
        }
        return w * h;
    }

    @Override
    public String toString() {
        return "Rectangle " + "Width = " + w + " Height = " + h + " Area is = " + this.calArea();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.shape;

/**
 *
 * @author ACER
 */
public class TestShape {

    public static void main(String[] args) {
        //
        Circle c1 = new Circle(1.5);
        Circle c2 = new Circle(4.5);
        Circle c3 = new Circle(5.5);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);

        //ทำต่อ
        //1
        Rectangle re1 = new Rectangle(5, 4);
        Rectangle re2 = new Rectangle(4, 4);
        re1.calArea();
        System.out.println(re1);
        re2.calArea();
        System.out.println(re2);

        Square sq1 = new Square(4);
        Square sq2 = new Square(6);
        sq1.calArea();
        System.out.println(sq1);
        sq2.calArea();
        System.out.println(sq2);
        //2

        Shape[] shapes = {c1, c2, c3, re1, re2, sq1, sq2};
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].getName() + "area :" + shapes[i].calArea());
        }

    }

}
